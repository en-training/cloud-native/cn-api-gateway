package et.gov.customs.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnApiGatewayApplication {

  public static void main(String[] args) {
    SpringApplication.run(CnApiGatewayApplication.class, args);
  }

}
